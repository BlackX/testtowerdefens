﻿using UnityEngine;
using StaffCommon;

namespace MonoTowerDefense.Rewind.Objects
{
	public class Spawner: MonoBehaviour, IWithState
	{
		[SerializeField]
		private MonoPoolTypes spawnObjectType = MonoPoolTypes.Goblin;
		[SerializeField]
		private float spawnTime = 1f;

		private Timer spawnTimer;


		private void Awake()
		{
			FramesManager.Singleton.RegisterPersisObject(this);
			spawnTimer = new Timer(Spawn, spawnTime);
		}

		private void Update()
		{
			spawnTimer.AddTime(Time.deltaTime);
		}

		private void OnValidate()
		{
			spawnTime = (spawnTime < 0.1f) ? 0.1f : spawnTime;
		}


		public int ID { get; set; }

		public object[] SaveState()
		{
			return new object[] { ID, spawnTimer.Time };
		}

		public void LoadState(object[] _data)
		{
			ID = (int)_data[0];
			spawnTimer.Time = (float)_data[1];
		}


		private void Spawn()
		{
			if (FramesManager.Singleton.CreateCurrentObjectFromPool(spawnObjectType) is Enemy _newEnemy)
			{
				_newEnemy.transform.position = transform.position;
				_newEnemy.UpdateTargetDistanse();
			}
			else
			{
				Debug.LogError($"Object manager return invalid enemy from type: {spawnObjectType}");
			}
		}
	}
}
