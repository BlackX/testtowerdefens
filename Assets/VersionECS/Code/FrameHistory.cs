﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace ECSTowerDefense
{
	public class FrameHistory
	{
		public int NewID => CurrentId++;
		public int CurrentId { get; private set; }
		public float FrameTime { get; private set; }
		public int FrameNumber { get; private set; }

		private readonly List<KeyValuePair<int, float>> framesData;
		private readonly Dictionary<int, ObjectHistory> objectHistory;


		public FrameHistory()
		{
			CurrentId = 0;
			FrameTime = 0;
			FrameNumber = 0;
			framesData = new List<KeyValuePair<int, float>>();
			objectHistory = new Dictionary<int, ObjectHistory>();
		}


		public void AddObjectHistory(ObjectHistory _object)
		{
			if (objectHistory.ContainsKey(_object.ID) == true)
			{
				Debug.LogError($"Object with ID {_object.ID} try add to history second time");
				return;
			}
			_object.SetStartFrame(FrameNumber);
			objectHistory.Add(_object.ID, _object);
		}

		public ObjectHistory GetObjectByID(int _id)
		{
			if (objectHistory.ContainsKey(_id) == false)
			{
				Debug.LogError($"Object with ID {_id} not fount in ObjectHistory");
				return default;
			}
			return objectHistory[_id];
		}

		public List<ObjectHistory> GetCurrentObjectsByType(EntityObjectTypes _type)
		{
			return objectHistory.Values.Where(x => x.Type == _type && x.CheckFrameNumber(FrameNumber) == true).ToList();
		}


		public void UpdateFrameData(float _deltaTime)
		{
			FrameNumber += 1;
			FrameTime += _deltaTime;
			framesData.Add(new KeyValuePair<int, float>(CurrentId, FrameTime));
		}

		public void SetCurrentFrame(float _normalizedTime)
		{
			FrameNumber = (int)(_normalizedTime * (framesData.Count - 1));
			FrameTime = framesData[FrameNumber].Value;
			CurrentId = framesData[FrameNumber].Key;
		}

		public void ClearHistoryAfterCurrentFrame()
		{
			var _query = objectHistory.Where(x => x.Value.StartFrame > FrameNumber).ToList();
			for (int i = 0; i < _query.Count; i++)
			{
				objectHistory.Remove(_query[i].Key);
			}

			while (framesData.Count > FrameNumber + 1)
			{
				framesData.Remove(framesData[framesData.Count - 1]);
			}

			objectHistory.Values.Where(x => x.CheckFrameNumber(FrameNumber) == true)
				.ToList().ForEach(x => x.SetCurrentFrame(FrameNumber));
		}
	}
}
