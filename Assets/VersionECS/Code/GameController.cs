﻿using ECSTowerDefense.Systems;
using Unity.Entities;
using UnityEngine;

namespace ECSTowerDefense
{
	public class GameController: Staff.MonoSingleton<GameController>
	{
		[SerializeField]
		private UiManager uiManager = default;
		[SerializeField]
		private Transform target = default;
		public Vector3 TargetPosition => target.position;

		public int NewID => FrameManager.NewID;
		public bool IsPause { get; private set; } = false;
		public FrameHistory FrameManager { get; private set; }
		public Entity DefaultBulletEntity { get; set; }

		private ComponentSystemBase BulletTranslator;
		private ComponentSystemBase EnemyTranslator;

		private ComponentSystemBase frameEnemesLoad;
		private ComponentSystemBase frameTowersLoad;
		private ComponentSystemBase frameBulletsLoad;
		private ComponentSystemBase frameSpawnersLoad;


		private void Update()
		{
			uiManager.SetTime(FrameManager.FrameTime);
		}

		private void OnValidate()
		{
			if (uiManager == null)
			{
				Debug.LogError("UiManager in GameController cannot be empty. Click me!", this);
			}
			if (target == null)
			{
				Debug.LogError("Target in GameController cannot be empty. Click me!", this);
			}
		}


		protected override void AwakeFunction()
		{
			FrameManager = new FrameHistory();

			BulletTranslator = World.DefaultGameObjectInjectionWorld.GetExistingSystem(typeof(BulletTranslateSystem));
			EnemyTranslator = World.DefaultGameObjectInjectionWorld.GetExistingSystem(typeof(EnemyTranslateSystem));

			frameEnemesLoad = World.DefaultGameObjectInjectionWorld.CreateSystem(typeof(FrameEmenesLoadSystem));
			frameTowersLoad = World.DefaultGameObjectInjectionWorld.CreateSystem(typeof(FrameTowersLoadSystem));
			frameBulletsLoad = World.DefaultGameObjectInjectionWorld.CreateSystem(typeof(FrameBulletLoadSystem));
			frameSpawnersLoad = World.DefaultGameObjectInjectionWorld.CreateSystem(typeof(FrameSpawnersLoadSystem));
		}

		protected override void DesrtoyFunction() { }


		public void Play()
		{
			FrameManager.ClearHistoryAfterCurrentFrame();

			IsPause = false;
			Time.timeScale = 1f;
			uiManager.ResetSlider();
			uiManager.SetPause(IsPause);
			EnemyTranslator.Enabled = true;
			BulletTranslator.Enabled = true;
		}

		public void Pause()
		{
			IsPause = true;
			Time.timeScale = 0f;
			uiManager.SetPause(IsPause);
			EnemyTranslator.Enabled = false;
			BulletTranslator.Enabled = false;
		}

		public void Restart()
		{
			SetFrameTime(0f);
			Play();
		}

		public void SetFrameTime(float _normalizedTime)
		{
			if (_normalizedTime < 0f || _normalizedTime > 1f)
			{
				Debug.LogError($"Slider time: {_normalizedTime} out of range [0, 1]");
				return;
			}
			FrameManager.SetCurrentFrame(_normalizedTime);
			uiManager.SetTime(FrameManager.FrameTime);

			frameEnemesLoad.Update();
			frameTowersLoad.Update();
			frameBulletsLoad.Update();
			frameSpawnersLoad.Update();
		}
	}
}
