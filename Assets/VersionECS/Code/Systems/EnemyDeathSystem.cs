﻿using Unity.Transforms;
using Unity.Entities;
using Unity.Jobs;

namespace ECSTowerDefense.Systems
{
	[UpdateInGroup(typeof(SimulationSystemGroup)), UpdateAfter(typeof(TransformSystemGroup))]
	public class EnemyDeathSystem: SystemBase
	{
		private EndSimulationEntityCommandBufferSystem simulationEntityCommandBuffer;


		protected override void OnCreate()
		{
			simulationEntityCommandBuffer = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
		}

		protected override void OnUpdate()
		{
			var comandBuffer = simulationEntityCommandBuffer.CreateCommandBuffer();

			Entities.WithAll<Enemy, AliveTag>().ForEach((Entity entity,
				in Health health,
				in AnimatorData animator) => {
					if (health.CurrenttHelth <= 0f)
					{
						comandBuffer.RemoveComponent(entity, typeof(AliveTag));
						animator.Animator.Play(animator.DeathAnimationName, 0, 0f);
					}
				}
			).WithName("EnemyDeathSystem").WithoutBurst().Run();
		}
	}
}
