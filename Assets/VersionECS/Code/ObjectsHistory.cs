﻿using System.Collections.Generic;
using Unity.Mathematics;
using Unity.Entities;

namespace ECSTowerDefense
{
	public class BulletHystory: ObjectHistory
	{
		public float Speed { get; private set; }
		public float Damage { get; private set; }
		public int TargetID { get; private set; }

		private List<float3> positions;


		public BulletHystory(int _id, EntityObjectTypes _type, float _speed, float _damage, int _targetId, int _createFrame = 0)
			: base(_id, _type, _createFrame)
		{
			Speed = _speed;
			Damage = _damage;
			TargetID = _targetId;

			positions = new List<float3>();
		}


		public void AddFrame(float3 _framePosition)
		{
			AddFrameValue(positions, _framePosition);
			UpdateFrame();
		}

		public float3 GetPositionInFrame(int _frameNumber)
		{
			return positions[_frameNumber - StartFrame];
		} 
	}


	public class SpawnerHystory: ObjectHistory
	{
		public float3 Position { get; private set; }
		public float SpawnTime { get; private set; }
		public EntityObjectTypes SpawnType { get; private set; }

		private List<float> timers;


		public SpawnerHystory(int _id, EntityObjectTypes _type, float3 _position, EntityObjectTypes _spawnType, float _targetTime,
			int _createFrame = 0): base(_id, _type, _createFrame)
		{
			Position = _position;
			SpawnType = _spawnType;
			SpawnTime = _targetTime;

			timers = new List<float>();
		}


		public void AddFrame(float _frameTimer)
		{
			AddFrameValue(timers, _frameTimer);
			UpdateFrame();
		}

		public float GetTimerInFrame(int _frameNumber)
		{
			return timers[_frameNumber - StartFrame];
		}
	}


	public class TowerHystory: ObjectHistory
	{
		public Entity BulletEntity { get; private set; }
		public float Incline { get; private set; }
		public float3 ShotPointOffset { get; private set; }

		public float Speed { get; private set; }
		public float Damage { get; private set; }
		public float Distance { get; private set; }
		public float3 Position { get; private set; }
		public float ShotTime { get; private set; }
		public EntityObjectTypes BulletType { get; private set; }

		private List<float> timers;
		private List<int> targetIds;


		public TowerHystory(int _id, EntityObjectTypes _type, Entity _bullet, float _incline, float3 _shotPointOffset,
			float _speed, float _damage, float _distance, float3 _position, float _targetTime, EntityObjectTypes _bulletType,
			int _createFrame = 0): base(_id, _type, _createFrame)
		{
			BulletEntity = _bullet;
			Incline = _incline;
			ShotPointOffset = _shotPointOffset;

			Speed = _speed;
			Damage = _damage;
			Distance = _distance;
			Position = _position;
			ShotTime = _targetTime;
			BulletType = _bulletType;

			timers = new List<float>();
			targetIds = new List<int>();
		}


		public void AddFrame(int _frameTergetId, float _frameTimer)
		{
			AddFrameValue(targetIds, _frameTergetId);
			AddFrameValue(timers, _frameTimer);
			UpdateFrame();
		}

		public float GetTimerInFrame(int _frameNumber)
		{
			return timers[_frameNumber - StartFrame];
		}

		public int GetTargetIdInFrame(int _frameNumber)
		{
			return targetIds[_frameNumber - StartFrame];
		}
	}


	public class EnemyHystory: ObjectHistory
	{
		private List<float> healths;
		private List<float3> positions;
		private List<float> animationTimes;


		public EnemyHystory(int _id, EntityObjectTypes _type, int _createFrame = 0)
			: base(_id, _type, _createFrame)
		{
			healths = new List<float>();
			positions = new List<float3>();
			animationTimes = new List<float>();
		}


		public void AddFrame(float3 _framePosition, float _animationTime, float _frameHealth)
		{
			AddFrameValue(healths, _frameHealth);
			AddFrameValue(positions, _framePosition);
			AddFrameValue(animationTimes, _animationTime);
			UpdateFrame();
		}

		public float GetHealthInFrame(int _frameNumber)
		{
			return healths[_frameNumber - StartFrame];
		}

		public float3 GetPositionInFrame(int _frameNumber)
		{
			return positions[_frameNumber - StartFrame];
		}

		public float GetAnimationTimeInFrame(int _frameNumber)
		{
			return animationTimes[_frameNumber - StartFrame];
		}
	}
}
