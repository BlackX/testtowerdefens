﻿using Unity.Entities;
using Unity.Jobs;

namespace ECSTowerDefense.Systems
{
	[UpdateInGroup(typeof(SimulationSystemGroup)), UpdateBefore(typeof(EndSimulationEntityCommandBufferSystem))]
	public class EnemyDestructionSystem: SystemBase
	{
		private EndSimulationEntityCommandBufferSystem simulationEntityCommandBuffer;


		protected override void OnCreate()
		{
			simulationEntityCommandBuffer = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
		}

		protected override void OnUpdate()
		{
			var comandBuffer = simulationEntityCommandBuffer.CreateCommandBuffer();

			Entities.WithAll<AliveTag>().ForEach((Entity entity,
				in Enemy enemy,
				in AnimatorData animator,
				in ObjectType type) => {
					if (enemy.DistanceToTarget <= 0f)
					{
						comandBuffer.RemoveComponent(entity, new ComponentTypes(typeof(InGameTag), typeof(AliveTag)));
						animator.Animator.gameObject.SetActive(false);
						Staff.PoolEntityObjects.Singleton.Push(entity, animator, type.Type);
					}
				}
			).WithName("AlivePassInEnemyDestructionSystem").WithoutBurst().Run();
			simulationEntityCommandBuffer.AddJobHandleForProducer(Dependency);

			Entities.WithNone<AliveTag>().WithAll<Enemy, InGameTag>().ForEach((Entity entity,
				in AnimatorData animator,
				in ObjectType type) => {
					if (animator.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1f)
					{
						comandBuffer.RemoveComponent(entity, typeof(InGameTag));
						animator.Animator.gameObject.SetActive(false);
						Staff.PoolEntityObjects.Singleton.Push(entity, animator, type.Type);
					}
				}
			).WithName("DeadPassInEnemyDestructionSystem").WithoutBurst().Run();
			simulationEntityCommandBuffer.AddJobHandleForProducer(Dependency);
		}
	}
}
