﻿using Unity.Mathematics;
using Unity.Transforms;
using Unity.Entities;
using Unity.Jobs;

namespace ECSTowerDefense.Systems
{
	[UpdateInGroup(typeof(TransformSystemGroup))]
	public class BulletTranslateSystem: SystemBase
	{
		private EndSimulationEntityCommandBufferSystem simulationEntityCommandBuffer;


		protected override void OnCreate()
		{
			simulationEntityCommandBuffer = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
		}

		protected override void OnUpdate()
		{
			var comandBuffer = simulationEntityCommandBuffer.CreateCommandBuffer();
			var translationType = GetComponentDataFromEntity<Translation>(true);
			var inGameType = GetComponentDataFromEntity<InGameTag>(true);
			var healthType = GetComponentDataFromEntity<Health>();
			var deltaTime = Time.DeltaTime;

			Entities.ForEach((Entity entity,
				ref Translation position,
				ref Rotation rotation,
				in Target target,
				in Bullet bullet,
				in VelocitySpeed speed) => {
					if (healthType.HasComponent(target.TargetValue) == false ||
						inGameType.HasComponent(target.TargetValue) == false ||
						translationType.HasComponent(target.TargetValue) == false)
					{
						comandBuffer.DestroyEntity(entity);
						return;
					}

					var targetPosition = translationType[target.TargetValue].Value;
					if (math.distance(translationType[target.TargetValue].Value, position.Value) > speed.SpeedValue * deltaTime)
					{
						rotation.Value = quaternion.LookRotationSafe(targetPosition - position.Value, math.up());
						position.Value += deltaTime * speed.SpeedValue * math.forward(rotation.Value);
					}
					else
					{
						var enemtHealth = healthType[target.TargetValue];
						enemtHealth.CurrenttHelth -= bullet.Damage;
						healthType[target.TargetValue] = enemtHealth;
						comandBuffer.DestroyEntity(entity);
					}
				}
			).WithReadOnly(inGameType).WithReadOnly(translationType)
			.WithName("BulletTranslateSystem").WithoutBurst().Run();
		}
	}
}
