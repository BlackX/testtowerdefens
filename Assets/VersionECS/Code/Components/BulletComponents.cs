﻿using Unity.Entities;

namespace ECSTowerDefense
{
	public struct Bullet: IComponentData
	{
		public int ID;
		public float Damage;
	}
}
