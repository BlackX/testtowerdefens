﻿using System.Collections.Generic;
using Unity.Transforms;
using Unity.Entities;
using UnityEngine;

namespace ECSTowerDefense
{
	[RequiresEntityConversion]
	public class TowerAuthoring: MonoBehaviour, IDeclareReferencedPrefabs, IConvertGameObjectToEntity
	{
		[SerializeField]
		private float attackDistance = 6f;
		[SerializeField]
		private float shotTime = 1f;
		[SerializeField]
		private float speed = 2f;
		[SerializeField]
		private float damage = 2f;
		[SerializeField, Range(-45f, 45f)]
		private float incline = 0f;
		[SerializeField]
		private Transform shotPoint = default;
		[SerializeField]
		private GameObject bulletPrefab = default;
		[SerializeField]
		private EntityObjectTypes bulletType = EntityObjectTypes.BulletCannonBall;
		[SerializeField]
		private EntityObjectTypes objectType = EntityObjectTypes.TowerCannon;

		private Vector3 shotPoinOffset;


		private void Awake()
		{
			shotPoinOffset = StaffFunctions.RotateAroundPoint(shotPoint.position, transform.position, transform.rotation);
			DestroyImmediate(shotPoint.gameObject);
		}

		private void OnValidate()
		{
			attackDistance = (attackDistance < 0.1f) ? 0.1f : attackDistance;
			shotTime = (shotTime < 0.1f) ? 0.1f : shotTime;
			damage = (damage < 0.1f) ? 0.1f : damage;
			speed = (speed < 0.1f) ? 0.1f : speed;

			if (shotPoint == null)
			{
				Debug.LogError("ShotPoint in Tower cannot be empty. Click me!", this);
			}
			else
			{
				shotPoint.parent.localRotation = Quaternion.Euler(incline, 0, shotPoint.parent.localRotation.z);
			}

			if (bulletPrefab == null)
			{
				Debug.LogError("BulletPrefab in Tower cannot be empty. Click me!", this);
			}
		}

		private void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.yellow;
			Gizmos.DrawWireSphere(transform.position, attackDistance);
		}


		public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
		{
			referencedPrefabs.Add(bulletPrefab);
		}

		public void Convert(Entity _entity, EntityManager _manager, GameObjectConversionSystem _system)
		{
			_manager.AddComponentData(_entity, new CopyTransformToGameObject());

			_manager.AddComponentData(_entity, new Timer {
				TargetTime = shotTime,
				CurrentTime = 0f
			});

			var id = GameController.Singleton.NewID;
			_manager.AddComponentData(_entity, new Tower {
				ID = id,
				Speed = speed,
				Damage = damage,
				AttackDistance = attackDistance,
				BulletEntity = _system.GetPrimaryEntity(bulletPrefab),
				BulletType = bulletType
			});

			_manager.AddComponentData(_entity, new TowerRotation {
				Incline = incline,
				ShotPointOffset = shotPoinOffset - transform.position
			});

			_manager.AddSharedComponentData(_entity, new ObjectType {
				Type = objectType
			});

			GameController.Singleton.DefaultBulletEntity = _system.GetPrimaryEntity(bulletPrefab);
			GameController.Singleton.FrameManager.AddObjectHistory(new TowerHystory (id, objectType,
				_system.GetPrimaryEntity(bulletPrefab),
				incline,
				shotPoinOffset - transform.position,
				speed,
				damage,
				attackDistance,
				transform.position,
				shotTime,
				bulletType
			));
		}
	}
}
