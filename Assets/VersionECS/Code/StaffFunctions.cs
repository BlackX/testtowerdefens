﻿using Unity.Mathematics;

namespace ECSTowerDefense
{
	public static class StaffFunctions
	{
		public static float3 RotateAroundPoint(float3 _position, float3 _pivot, float3 _axis, float _delta)
		{
			return math.mul(quaternion.AxisAngle(_axis, _delta), _position - _pivot) + _pivot;
		}


		public static float3 RotateAroundPoint(float3 _position, float3 _pivot, quaternion _quaternion)
		{
			return math.mul(_quaternion, _position - _pivot) + _pivot;
		}


		public static quaternion GetYRotationOnTarget(float3 direction, float angleX = 0f, float angleZ = 0f)
		{
			return quaternion.EulerXYZ(
				angleX,
				math.PI * 1.5f - math.sign(direction.z) * math.acos(direction.x / math.length(direction)),
				angleZ);
		}
	}
}
