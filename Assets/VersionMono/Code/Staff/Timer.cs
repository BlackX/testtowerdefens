﻿using System;

namespace StaffCommon
{
	public class Timer
	{
		private readonly float targetTime;
		private readonly Action timerAction;

		private float time;
		public float Time
		{
			get => time;
			set => AddTime(value - time);
		}


		public Timer(Action _timerAction, float _targetTime, float _startTime = 0f)
		{
			timerAction = _timerAction;
			targetTime = _targetTime;
			time = _startTime;
		}


		public void AddTime(float _time)
		{
			time += _time;
			if (Time >= targetTime)
			{
				while (Time > targetTime)
				{
					time -= targetTime;
					timerAction?.Invoke();
				}
			}
		}
	}
}
