﻿using Unity.Entities;

namespace ECSTowerDefense
{
	public struct Spawner: IComponentData
	{
		public int ID;
		public EntityObjectTypes SpawnObjectType;
	}
}
