﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace StaffCommon
{
	public class PoolMonoObjects: MonoBehaviour
	{
		[System.Serializable]
		private class PoolObjectSettings
		{
			[SerializeField]
			private MonoPoolTypes objectType = default;
			public MonoPoolTypes ObjectType => objectType;
			[SerializeField]
			private GameObject objectPrefab = default;
			public GameObject ObjectPrefab => objectPrefab;
			[SerializeField]
			private int count = default;
			public int Count => count;
		}


		[SerializeField]
		private List<PoolObjectSettings> objectSettings = default;

		private Dictionary<MonoPoolTypes, Stack<IPoolMonoObject>> poolDictionary;
		private Dictionary<MonoPoolTypes, PoolObjectSettings> settingsDictionary;


		private void Awake()
		{
			poolDictionary = new Dictionary<MonoPoolTypes, Stack<IPoolMonoObject>>();
			settingsDictionary = new Dictionary<MonoPoolTypes, PoolObjectSettings>();

			foreach (var _poolObject in objectSettings)
			{
				settingsDictionary.Add(_poolObject.ObjectType, _poolObject);
				poolDictionary.Add(_poolObject.ObjectType, new Stack<IPoolMonoObject>());
				for (int i = 0; i < _poolObject.Count; i++)
				{
					var _newObject = CreatePoolObject(_poolObject.ObjectPrefab);
					_newObject.Mono.gameObject.SetActive(false);
					poolDictionary[_poolObject.ObjectType].Push(_newObject);
				}
			}
		}

		private void OnValidate()
		{
			if (objectSettings == null || objectSettings.Select(x => x.ObjectType).Distinct().Count() != objectSettings.Count)
			{
				Debug.LogError("ObjectSettings in Pool cannot be empty or contain non-unique types. Click me!", this);
				return;
			}

			var _invalidSetting = objectSettings.FirstOrDefault(x => x.ObjectPrefab == null);
			if (_invalidSetting != null)
			{
				Debug.LogError($"Type ({_invalidSetting.ObjectType}) in ObjectSettings in Pool have null prefab. Click me!", this);
			}

			_invalidSetting = objectSettings.FirstOrDefault(x => x.Count < 0);
			if (_invalidSetting != null)
			{
				Debug.LogError($"Type ({_invalidSetting.ObjectType}) in ObjectSettings in Pool have negative count. Click me!", this);
			}
		}


		public IPoolMonoObject Pull(MonoPoolTypes _type, Transform _parent)
		{
			if (poolDictionary.ContainsKey(_type) == false)
			{
				Debug.LogError($"Unknown pool object type {_type} for pool settings");
				return null;
			}

			IPoolMonoObject _object;
			if (poolDictionary[_type].Count > 0)
			{
				_object = poolDictionary[_type].Pop();
			}
			else
			{
				_object = CreatePoolObject(settingsDictionary[_type].ObjectPrefab);
			}

			_object.Mono.transform.parent = _parent;
			_object.Mono.gameObject.SetActive(true);
			_object.AfterPull();
			return _object;
		}

		public void Push(IPoolMonoObject _object)
		{
			_object.Mono.transform.parent = transform;
			_object.Mono.gameObject.SetActive(false);

			if (poolDictionary.ContainsKey(_object.ObjectType) == false)
			{
				Debug.LogError($"Unknown pool object type {_object.ObjectType} for pool settings");
				return;
			}

			if (poolDictionary[_object.ObjectType].Count < settingsDictionary[_object.ObjectType].Count)
			{
				poolDictionary[_object.ObjectType].Push(_object);
			}
			else
			{
				Destroy(_object.Mono.gameObject);
			}
		}


		private IPoolMonoObject CreatePoolObject(GameObject _prefab)
		{
			return Instantiate(_prefab, transform).GetComponent<IPoolMonoObject>();
		}
	}
}
