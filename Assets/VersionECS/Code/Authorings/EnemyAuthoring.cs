﻿using Unity.Transforms;
using Unity.Entities;
using UnityEngine;

namespace ECSTowerDefense
{
	[RequiresEntityConversion]
	public class EnemyAuthoring: MonoBehaviour, IConvertGameObjectToEntity
	{
		[SerializeField]
		private float speed = 1f;
		[SerializeField]
		private float health = 3f;
		[SerializeField]
		private float animationSpeed = 0.75f;
		[SerializeField, Range(-180f, 180f)]
		private float startRotate = 0f;
		[SerializeField]
		private string runAnimationName = default;
		[SerializeField]
		private string deathAnimationName = default;
		[SerializeField]
		private Vector2 minMaxStartAnimationTime = default;
		[SerializeField]
		private EntityObjectTypes objectType = EntityObjectTypes.GoblinEnemy;
		[SerializeField]
		private Transform heartPoint = default;
		[SerializeField]
		private Animator animator = default;


		private void OnValidate()
		{
			speed = (speed < 0.1f) ? 0.1f : speed;
			health = (health < 0.1f) ? 0.1f : health;
			animationSpeed = (animationSpeed < 0.1f) ? 0.1f : animationSpeed;

			if (minMaxStartAnimationTime.y < 0f || minMaxStartAnimationTime.x > minMaxStartAnimationTime.y)
			{
				Debug.LogError("minMaxStartAnimationTime in Enemy cannot be negative. Click me!", this);
			}

			if (string.IsNullOrEmpty(runAnimationName) == true)
			{
				Debug.LogError("RunAnimationName in Enemy cannot be empty. Click me!", this);
			}
			if (string.IsNullOrEmpty(deathAnimationName) == true)
			{
				Debug.LogError("DeathAnimationName in Enemy cannot be empty. Click me!", this);
			}
			if (heartPoint == null)
			{
				Debug.LogError("HeartPoint in Enemy cannot be empty. Click me!", this);
			}
			if (animator == null)
			{
				Debug.LogError("Animator in Enemy cannot be empty. Click me!", this);
			}

			transform.localRotation = Quaternion.Euler(transform.localRotation.x, startRotate, transform.localRotation.z);
		}


		public void Convert(Entity _entity, EntityManager _manager, GameObjectConversionSystem _system)
		{
			_manager.AddComponentData(_entity, new CopyTransformToGameObject());

			_manager.AddComponentData(_entity, new VelocitySpeed {
				SpeedValue = speed
			});

			_manager.AddComponentData(_entity, new Enemy {
				ID = -1,
				DistanceToTarget = -1
			});

			_manager.AddComponentData(_entity, new Health {
				StartHelth = health,
				CurrenttHelth = health
			});

			_manager.AddComponentData(_entity, new HeartOffset {
				Offset = heartPoint.position - transform.position
			});

			var _animatorData = new AnimatorData {
				Animator = animator,
				RunAnimationName = runAnimationName,
				DeathAnimationName = deathAnimationName,
				RandomStartAnimationTime = Random.Range(minMaxStartAnimationTime.x, minMaxStartAnimationTime.y) % 1f
			};
			_manager.AddComponentData(_entity, _animatorData);

			_manager.AddSharedComponentData(_entity, new ObjectType {
				Type = objectType
			});

			animator.speed = animationSpeed;
			_animatorData.Animator.gameObject.SetActive(false);
			Staff.PoolEntityObjects.Singleton.Push(_entity, _animatorData, objectType);
		}
	}
}
