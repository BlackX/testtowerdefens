﻿using Unity.Mathematics;
using Unity.Entities;
using UnityEngine;

namespace ECSTowerDefense
{
	public struct Enemy: IComponentData
	{
		public int ID;
		public float DistanceToTarget;
	}


	public struct HeartOffset: IComponentData
	{
		public float3 Offset;
	}


	public class AnimatorData: IComponentData
	{
		public Animator Animator;
		public string RunAnimationName;
		public string DeathAnimationName;
		public float RandomStartAnimationTime;
	}
}
