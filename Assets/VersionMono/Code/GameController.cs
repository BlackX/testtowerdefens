﻿using UnityEngine;
using StaffCommon;
using MonoTowerDefense.Rewind;

namespace MonoTowerDefense
{
	public class GameController: MonoSingleton<GameController>
	{
		[SerializeField]
		private UiManager uiManager = default;
		[SerializeField]
		private Transform target = default;

		public Vector3 TargetPosition => target.position;
		public bool IsPause { get; private set; } = false;


		private void Update()
		{
			uiManager.SetTime(FramesManager.Singleton.FrameTime);
		}

		private void OnValidate()
		{
			if (uiManager == null)
			{
				Debug.LogError("UiManager in GameController cannot be empty. Click me!", this);
			}
			if (target == null)
			{
				Debug.LogError("Target in GameController cannot be empty. Click me!", this);
			}
		}


		protected override void AwakeFunction() { }

		protected override void DesrtoyFunction() { }


		public void Play()
		{
			FramesManager.Singleton.ClearHistoryAfterCurrentFrame();

			IsPause = false;
			Time.timeScale = 1f;
			uiManager.ResetSlider();
			uiManager.SetPause(IsPause);
		}

		public void Pause()
		{
			IsPause = true;
			Time.timeScale = 0f;
			uiManager.SetPause(IsPause);
		}

		public void Restart()
		{
			SetFrameTime(0f);
			Play();
		}

		public void SetFrameTime(float _normalizedTime)
		{
			if (_normalizedTime < 0f || _normalizedTime > 1f)
			{
				Debug.LogError($"Slider time: {_normalizedTime} out of range [0, 1]");
				return;
			}
			FramesManager.Singleton.SetCurrentFrame(_normalizedTime);
			uiManager.SetTime(FramesManager.Singleton.FrameTime);
		}
	}
}
