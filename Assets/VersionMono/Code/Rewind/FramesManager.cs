﻿using System;
using System.Collections.Generic;
using UnityEngine;
using StaffCommon;

namespace MonoTowerDefense.Rewind
{
	public class FramesManager: MonoSingleton<FramesManager>
	{
		[SerializeField]
		private PoolMonoObjects pool = default;

		public float FrameTime { get; private set; }

		private int currentId;
		private int frameNumber;
		private List<FrameState> frameHistory;
		private List<IWithState> persistObjects;
		private Dictionary<MonoPoolTypes, List<IWithState>> currentObject;


		private void LateUpdate()
		{
			if (GameController.Singleton.IsPause == true)
			{
				return;
			}

			FrameTime += Time.deltaTime;
			var _persistStates = new List<object[]>();
			for (int i = 0; i < persistObjects.Count; i++)
			{
				_persistStates.Add(persistObjects[i].SaveState());
			}

			var _objectStates = new Dictionary<MonoPoolTypes, List<object[]>>();
			foreach (var _objectType in currentObject.Keys)
			{
				_objectStates[_objectType] = new List<object[]>();
				for (int i = 0; i < currentObject[_objectType].Count; i++)
				{
					_objectStates[_objectType].Add(currentObject[_objectType][i].SaveState());
				}
			}
			frameHistory.Add(new FrameState(_persistStates, _objectStates, FrameTime));
			frameNumber = frameHistory.Count;
		}

		private void OnValidate()
		{
			if (pool == null)
			{
				Debug.LogError("Pool in ObjectManager cannot be empty. Click me!", this);
			}
		}


		protected override void AwakeFunction()
		{
			currentId = 0;
			FrameTime = 0f;
			frameNumber = 0;

			frameHistory = new List<FrameState>();
			persistObjects = new List<IWithState>();
			currentObject = new Dictionary<MonoPoolTypes, List<IWithState>>();

			foreach (Enum _value in Enum.GetValues(typeof(MonoPoolTypes)))
			{
				currentObject.Add((MonoPoolTypes)_value, new List<IWithState>());
			}
		}

		protected override void DesrtoyFunction() { }


		public void RegisterPersisObject(IWithState _object)
		{
			_object.ID = currentId++;
			persistObjects.Add(_object);
		}

		public void SetCurrentFrame(float _normalizedTime)
		{
			var _frameNumber = (int)(_normalizedTime * (frameHistory.Count - 1));

			if (frameHistory[_frameNumber].PersistStates.Count != persistObjects.Count)
			{
				Debug.LogError("Persistent objects count is changed");
				return;
			}

			frameNumber = _frameNumber;
			FrameTime = frameHistory[frameNumber].FrameTime;
			for (int i = 0; i < frameHistory[frameNumber].PersistStates.Count; i++)
			{
				persistObjects[i].LoadState(frameHistory[frameNumber].PersistStates[i]);
			}

			foreach (var _objectType in frameHistory[frameNumber].ObjectStates.Keys)
			{
				UpdateObjectsWithTypeFromFrame(_objectType, frameNumber);
			}
		}

		public IWithState CreateCurrentObjectFromPool(MonoPoolTypes _poolType)
		{
			var _newObject = AddCurrentObjectFromPool(_poolType);
			_newObject.ID = currentId++;
			return _newObject;
		}

		public void ReturnObjectToPool(IPoolMonoObject _object)
		{
			if (_object is IWithState _objectWithState)
			{
				DelCurrentObject(_object.ObjectType, _objectWithState);
				pool.Push(_object);
			}
			else
			{
				Debug.LogError("Found pool object without IWithState interface");
			}
		}

		public List<IWithState> GetObjectsByType(MonoPoolTypes _type)
		{
			if (currentObject.ContainsKey(_type) == false || currentObject[_type].Count == 0)
			{
				return null;
			}
			return currentObject[_type];
		}

		public void ClearHistoryAfterCurrentFrame()
		{
			while (frameHistory.Count > frameNumber + 1)
			{
				frameHistory.Remove(frameHistory[frameHistory.Count - 1]);
			}
		}


		private IWithState AddCurrentObjectFromPool(MonoPoolTypes _poolType)
		{
			if (pool.Pull(_poolType, transform) is IWithState _object)
			{
				AddCurrentObject(_poolType, _object);
				return _object;
			}
			else
			{
				Debug.LogError($"Poll return invalid object from type {_poolType}");
				return null;
			}
		}

		private void AddCurrentObject(MonoPoolTypes _type, IWithState _object)
		{
			if (currentObject.ContainsKey(_type) == false)
			{
				currentObject[_type] = new List<IWithState>();
			}

			if (currentObject[_type].Contains(_object) == true)
			{
				Debug.LogWarning("Try add object in current object list second time");
				return;
			}
			currentObject[_type].Add(_object);
		}

		private void DelCurrentObject(MonoPoolTypes _type, IWithState _object)
		{
			if (currentObject.ContainsKey(_type) == false)
			{
				Debug.LogError($"Try remove object from current objects with Unknown type {_object.GetType()}");
				return;
			}
			if (currentObject[_type].Remove(_object) == false)
			{
				Debug.LogWarning("Not found object in current object list for remove");
			}
		}

		private void UpdateObjectsWithTypeFromFrame(MonoPoolTypes _objectType, int _frameNumber)
		{
			var _objectStates = frameHistory[_frameNumber].ObjectStates[_objectType];

			for (int i = 0; i < _objectStates.Count; i++)
			{
				if (i < currentObject[_objectType].Count)
				{
					currentObject[_objectType][i].LoadState(_objectStates[i]);
				}
				else
				{
					(AddCurrentObjectFromPool(_objectType)).LoadState(_objectStates[i]);
				}
			}

			while (currentObject[_objectType].Count > _objectStates.Count)
			{
				ReturnObjectToPool((IPoolMonoObject)currentObject[_objectType][currentObject[_objectType].Count - 1]);
			}
		}
	}
}
