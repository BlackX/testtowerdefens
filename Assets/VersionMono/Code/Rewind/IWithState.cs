﻿
namespace MonoTowerDefense.Rewind
{
	public interface IWithState
	{
		int ID { get; set; }
		object[] SaveState();
		void LoadState(object[] _data);
	}
}
