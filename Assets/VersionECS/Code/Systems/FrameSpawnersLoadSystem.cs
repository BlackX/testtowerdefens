﻿using System.Linq;
using Unity.Transforms;
using Unity.Entities;

namespace ECSTowerDefense.Systems
{
	[DisableAutoCreation]
	public class FrameSpawnersLoadSystem: SystemBase
	{
		protected override void OnUpdate()
		{
			var frameManager = GameController.Singleton.FrameManager;
			var frameEnemes = frameManager.GetCurrentObjectsByType(EntityObjectTypes.TimerSpawner)
				.Select(x => (SpawnerHystory)x).ToList();

			Entities.WithSharedComponentFilter(new ObjectType { Type = EntityObjectTypes.TimerSpawner })
				.ForEach((int entityInQueryIndex,
				ref Timer timer,
				ref Spawner spawner,
				ref Translation position) => {
					spawner.ID = frameEnemes[entityInQueryIndex].ID;
					spawner.SpawnObjectType = frameEnemes[entityInQueryIndex].SpawnType;

					position.Value = frameEnemes[entityInQueryIndex].Position;

					timer.TargetTime = frameEnemes[entityInQueryIndex].SpawnTime;
					timer.CurrentTime = frameEnemes[entityInQueryIndex].GetTimerInFrame(frameManager.FrameNumber);
				}
			).WithName("FrameSpawnerLoadSystem").WithoutBurst().Run();
		}
	}
}
