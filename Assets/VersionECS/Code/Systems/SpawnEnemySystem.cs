﻿using Unity.Mathematics;
using Unity.Transforms;
using Unity.Entities;
using UnityEngine;

namespace ECSTowerDefense.Systems
{
	[UpdateInGroup(typeof(SimulationSystemGroup)), UpdateAfter(typeof(TransformSystemGroup))]
	public class SpawnEnemySystem: SystemBase
	{
		private EndSimulationEntityCommandBufferSystem simulationEntityCommandBuffer;


		protected override void OnCreate()
		{
			simulationEntityCommandBuffer = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
		}

		protected override void OnUpdate()
		{
			var enemyType = GetComponentDataFromEntity<Enemy>();
			var healthType = GetComponentDataFromEntity<Health>();
			var translationType = GetComponentDataFromEntity<Translation>();

			var comandBuffer = simulationEntityCommandBuffer.CreateCommandBuffer();

			Entities.ForEach((Entity _entity,
				ref NewAnimatedObject animationObject) => {
					if (animationObject.Type == EntityObjectTypes.GoblinEnemy)
					{
						var (enemyEntity, animatorData) = Staff.PoolEntityObjects.Singleton.Pull(animationObject.Type);

						if (translationType.HasComponent(enemyEntity) == false ||
							healthType.HasComponent(enemyEntity) == false ||
							enemyType.HasComponent(enemyEntity) == false)
						{
							Debug.LogError($"Translation or Health or Enemy not found on Enemy entity {enemyEntity.Index}");
							return;
						}

						var translation = translationType[enemyEntity];
						translation.Value = animationObject.Position;
						translationType[enemyEntity] = translation;

						var health = healthType[enemyEntity];
						health.CurrenttHelth = health.StartHelth;
						healthType[enemyEntity] = health;

						var id = GameController.Singleton.NewID;
						var enemy = enemyType[enemyEntity];
						enemy.ID = id;
						enemy.DistanceToTarget = math.distance(animationObject.Position, GameController.Singleton.TargetPosition);
						enemyType[enemyEntity] = enemy;

						animatorData.Animator.gameObject.SetActive(true);
						animatorData.Animator.Play(animatorData.RunAnimationName, 0, animatorData.RandomStartAnimationTime);

						GameController.Singleton.FrameManager.AddObjectHistory(new EnemyHystory(id, animationObject.Type));

						comandBuffer.AddComponent(enemyEntity, new ComponentTypes(typeof(InGameTag), typeof(AliveTag)));
						comandBuffer.DestroyEntity(_entity);
					}
				}
			).WithName("EnemySpawnSystem").WithoutBurst().Run();
		}
	}
}
