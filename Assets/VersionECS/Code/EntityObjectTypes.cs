﻿
namespace ECSTowerDefense
{
	public enum EntityObjectTypes
	{
		GoblinEnemy,
		BulletCannonBall,
		TowerCannon,
		TimerSpawner
	}
}
