﻿using System.Linq;
using UnityEngine;
using StaffCommon;

namespace MonoTowerDefense.Rewind.Objects
{
	public class Tower: MonoBehaviour, IWithState
	{
		[SerializeField]
		private float shotTime = 1f;
		[SerializeField]
		private float attackDistance = 6f;
		[SerializeField]
		private float damage = 2f;
		[SerializeField, Range(-45f, 45f)]
		private float incline = 0f;
		[SerializeField]
		private Transform shotPoint = default;
		[SerializeField]
		private MonoPoolTypes bulletObjectType = MonoPoolTypes.CannonBall;
		[SerializeField]
		private MonoPoolTypes enemyObjectType = MonoPoolTypes.Goblin;

		private Timer shotTimer;
		private Enemy currentTarget;

		private float sqrDistanse;


		private void Awake()
		{
			FramesManager.Singleton.RegisterPersisObject(this);

			shotTimer = new Timer(Shot, shotTime);
			sqrDistanse = attackDistance * attackDistance;
		}

		private void Update()
		{
			CheckCurrentTarget();
			shotTimer.AddTime(Time.deltaTime);
			shotPoint.parent.rotation = Quaternion.Euler(incline, GetTargetAngle(), 0f);
		}

		private void OnValidate()
		{
			shotPoint.parent.localRotation = Quaternion.Euler(incline, 0f, shotPoint.parent.localRotation.z);

			attackDistance = (attackDistance < 0.1f) ? 0.1f : attackDistance;
			shotTime = (shotTime < 0.1f) ? 0.1f : shotTime;
			damage = (damage < 0.1f) ? 0.1f : damage;

			if (shotPoint == null)
			{
				Debug.LogError("ShotPoint in Tower cannot be empty. Click me!", this);
			}
		}

		private void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.yellow;
			Gizmos.DrawWireSphere(transform.position, attackDistance);
		}


		public int ID { get; set; }

		public object[] SaveState()
		{
			return new object[] { ID, currentTarget, shotTimer.Time };
		}

		public void LoadState(object[] _data)
		{
			ID = (int)_data[0];
			currentTarget = (Enemy)_data[1];
			shotTimer.Time = (float)_data[2];
		}


		private void Shot()
		{
			if (currentTarget != null)
			{
				if (FramesManager.Singleton.CreateCurrentObjectFromPool(bulletObjectType) is Bullet _newBullet)
				{
					_newBullet.transform.position = shotPoint.position;
					_newBullet.SetParametrs(damage, currentTarget);
				}
				else
				{
					Debug.LogError($"Object manager return invalid bullet from type: {bulletObjectType}");
				}
			}
		}

		private void CheckCurrentTarget()
		{
			if (currentTarget == null || currentTarget.gameObject.activeSelf == true || CheckDistance(currentTarget) == false)
			{
				var _allEnemy = FramesManager.Singleton.GetObjectsByType(enemyObjectType);
				if (_allEnemy != null)
				{
					currentTarget = (Enemy)_allEnemy.FirstOrDefault(x => CheckDistance((Enemy)x));
				}
				else
				{
					currentTarget = null;
				}
			}
		}

		private bool CheckDistance(Enemy _enemy)
		{
			return _enemy.Alive == true && (_enemy.transform.position - transform.position).sqrMagnitude < sqrDistanse;
		}

		private float GetTargetAngle()
		{
			if (currentTarget != null)
			{
				var _direction = transform.position - currentTarget.transform.position;
				return 270f - Mathf.Sign(_direction.z) * Mathf.Acos(_direction.x / _direction.magnitude) * 180f / Mathf.PI;
			}
			return transform.rotation.eulerAngles.y;
		}
	}
}
