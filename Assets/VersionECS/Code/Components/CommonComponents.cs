﻿using Unity.Mathematics;
using Unity.Entities;

namespace ECSTowerDefense
{
	public struct InGameTag: IComponentData { }


	public struct AliveTag: IComponentData { }


	public struct Target: IComponentData
	{
		public Entity TargetValue;
		public int CurrentTargetID;
	}


	public struct VelocitySpeed: IComponentData
	{
		public float SpeedValue;
	}


	public struct Health: IComponentData
	{
		public float StartHelth;
		public float CurrenttHelth;
	}


	public struct Timer: IComponentData
	{
		public float TargetTime;
		public float CurrentTime;
	}


	public struct NewAnimatedObject: IComponentData
	{
		public EntityObjectTypes Type;
		public float3 Position;
	}


	public struct ObjectType: ISharedComponentData
	{
		public EntityObjectTypes Type;
	}
}
