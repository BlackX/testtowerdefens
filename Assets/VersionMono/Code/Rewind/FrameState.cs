﻿using System.Collections.Generic;
using StaffCommon;

namespace MonoTowerDefense.Rewind
{
	public class FrameState
	{
		public Dictionary<MonoPoolTypes, List<object[]>> ObjectStates { get; }
		public List<object[]> PersistStates { get; }
		public float FrameTime { get; }


		public FrameState(List<object[]> _persistStates, Dictionary<MonoPoolTypes, List<object[]>> _objectsStates, float _frameTime)
		{
			PersistStates = _persistStates;
			ObjectStates = _objectsStates;
			FrameTime = _frameTime;
		}
	}
}
