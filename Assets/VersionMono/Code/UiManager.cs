﻿using UnityEngine;
using UnityEngine.UI;

namespace MonoTowerDefense
{
	public class UiManager: MonoBehaviour
	{
		[SerializeField]
		private Text time = default;
		[SerializeField]
		private Button play = default;
		[SerializeField]
		private Button pause = default;
		[SerializeField]
		private Button restart = default;
		[SerializeField]
		private Slider frameSlider = default;


		private void Awake()
		{
			SetPause(false);
			SetTime(0f);
		}

		private void OnValidate()
		{
			if (time == null)
			{
				Debug.LogError("Time in UiManager cannot be empty. Click me!", this);
			}
			if (play == null)
			{
				Debug.LogError("Play in UiManager button cannot be empty. Click me!", this);
			}
			if (pause == null)
			{
				Debug.LogError("Pause in UiManager button cannot be empty. Click me!", this);
			}
			if (restart == null)
			{
				Debug.LogError("Restart in UiManager button cannot be empty. Click me!", this);
			}
			if (frameSlider == null)
			{
				Debug.LogError("Slider in UiManager button cannot be empty. Click me!", this);
			}
		}


		public void SetPause(bool _pause)
		{
			pause.gameObject.SetActive(!_pause);
			restart.gameObject.SetActive(!_pause);

			play.gameObject.SetActive(_pause);
			frameSlider.gameObject.SetActive(_pause);
		}

		public void SetTime(float _time)
		{
			time.text = $"Time: {_time:0.00}";
		}

		public void ResetSlider()
		{
			frameSlider.value = frameSlider.maxValue;
		}


		public void ButtonPlay()
		{
			GameController.Singleton.Play();
		}

		public void ButtonPause()
		{
			GameController.Singleton.Pause();
		}

		public void ButtonRestart()
		{
			GameController.Singleton.Restart();
		}

		public void SliderUpdateTime(float _value)
		{
			GameController.Singleton.SetFrameTime(_value);
		}
	}
}
