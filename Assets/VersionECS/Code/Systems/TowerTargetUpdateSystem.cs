﻿using Unity.Mathematics;
using Unity.Transforms;
using Unity.Entities;
using Unity.Jobs;

namespace ECSTowerDefense.Systems
{
	[UpdateInGroup(typeof(InitializationSystemGroup))]
	public class TowerTargetUpdateSystem: SystemBase
	{
		private EntityQuery query;
		private EndInitializationEntityCommandBufferSystem initializationCommandBuffer;

		
		protected override void OnCreate()
		{
			query = GetEntityQuery(
				typeof(Enemy),
				typeof(Translation),
				ComponentType.ReadOnly<AliveTag>());
			initializationCommandBuffer = World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
		}

		protected override void OnUpdate()
		{
			var aliveType = GetComponentDataFromEntity<AliveTag>(true);
			var translationType = GetComponentDataFromEntity<Translation>(true);
			var comandBuffer = initializationCommandBuffer.CreateCommandBuffer().AsParallelWriter();

			var entites = query.ToEntityArray(Unity.Collections.Allocator.TempJob);
			var enemes = query.ToComponentDataArray<Enemy>(Unity.Collections.Allocator.TempJob);
			var positions = query.ToComponentDataArray<Translation>(Unity.Collections.Allocator.TempJob);

			Dependency = Entities.WithNone<Target>().ForEach((Entity entity, int entityInQueryIndex,
				in Translation position,
				in Tower tower) => {
					for (int i = 0; i < positions.Length; i++)
					{
						if (math.distance(positions[i].Value, position.Value) < tower.AttackDistance)
						{
							comandBuffer.AddComponent<Target>(entityInQueryIndex, entity);
							comandBuffer.SetComponent(entityInQueryIndex, entity, new Target {
								CurrentTargetID = enemes[i].ID,
								TargetValue = entites[i]
							});
							return;
						}
					}
				})
				.WithReadOnly(enemes).WithReadOnly(entites).WithReadOnly(positions)
				.WithName("FindPassInTowerTargetUpdateSystem").ScheduleParallel(Dependency);
			initializationCommandBuffer.AddJobHandleForProducer(Dependency);

			Dependency = Entities.ForEach((Entity entity, int entityInQueryIndex,
				ref Target target,
				in Translation position,
				in Tower tower) => {
					if (aliveType.HasComponent(target.TargetValue) == false ||
						math.distance(translationType[target.TargetValue].Value, position.Value) > tower.AttackDistance)
					{
						for (int i = 0; i < positions.Length; i++)
						{
							if (math.distance(positions[i].Value, position.Value) <= tower.AttackDistance)
							{
								target.CurrentTargetID = enemes[i].ID;
								target.TargetValue = entites[i];
								return;
							}
						}
						comandBuffer.RemoveComponent<Target>(entityInQueryIndex, entity);
					}
				})
				.WithReadOnly(translationType).WithReadOnly(aliveType)
				.WithReadOnly(enemes).WithDisposeOnCompletion(enemes)
				.WithReadOnly(entites).WithDisposeOnCompletion(entites)
				.WithReadOnly(positions).WithDisposeOnCompletion(positions)
				.WithName("UpdatePassInTowerTargetUpdateSystem").ScheduleParallel(Dependency);
			initializationCommandBuffer.AddJobHandleForProducer(Dependency);
		}
	}
}
