﻿using Unity.Transforms;
using Unity.Entities;
using Unity.Jobs;

namespace ECSTowerDefense.Systems
{
	[UpdateInGroup(typeof(InitializationSystemGroup))]
	public class SpawnTimerSystem: SystemBase
	{
		private EndInitializationEntityCommandBufferSystem initializationCommandBuffer;


		protected override void OnCreate()
		{
			initializationCommandBuffer = World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
		}

		protected override void OnUpdate()
		{
			var comandBuffer = initializationCommandBuffer.CreateCommandBuffer().AsParallelWriter();
			var newArchetype = EntityManager.CreateArchetype(typeof(NewAnimatedObject));
			var deltaTime = Time.DeltaTime;

			Entities.ForEach((int entityInQueryIndex,
				ref Timer timer,
				in Translation position,
				in Spawner spawner) => {
					timer.CurrentTime += deltaTime;
					if (timer.CurrentTime >= timer.TargetTime)
					{
						for (int i = 0; i < (int)(timer.CurrentTime / timer.TargetTime); i++)
						{
							var newEntity = comandBuffer.CreateEntity(entityInQueryIndex, newArchetype);

							comandBuffer.SetComponent(entityInQueryIndex, newEntity,
								new NewAnimatedObject {
									Position = position.Value,
									Type = spawner.SpawnObjectType
								}
							);
						}
						timer.CurrentTime %= timer.TargetTime;
					}
				}
			).WithName("SpawnTimerSystem").ScheduleParallel();

			initializationCommandBuffer.AddJobHandleForProducer(Dependency);
		}
	}
}
