﻿using Unity.Mathematics;
using Unity.Entities;

namespace ECSTowerDefense
{
	public struct Tower: IComponentData
	{
		public int ID;
		public float Speed;
		public float Damage;
		public float AttackDistance;
		public Entity BulletEntity;
		public EntityObjectTypes BulletType;
	}


	public struct TowerRotation: IComponentData
	{
		public float Incline;
		public float3 ShotPointOffset;
		public float3 CurrentShotPoint;
	}
}
