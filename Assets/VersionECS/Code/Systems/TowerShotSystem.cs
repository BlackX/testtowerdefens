﻿using Unity.Transforms;
using Unity.Entities;
using Unity.Jobs;

namespace ECSTowerDefense.Systems
{
	[UpdateInGroup(typeof(SimulationSystemGroup)), UpdateAfter(typeof(TransformSystemGroup))]
	public class TowerShotSystem: SystemBase
	{
		private EndSimulationEntityCommandBufferSystem simulationEntityCommandBuffer;


		protected override void OnCreate()
		{
			simulationEntityCommandBuffer = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
		}

		protected override void OnUpdate()
		{
			var comandBuffer = simulationEntityCommandBuffer.CreateCommandBuffer();

			Entities.ForEach((
				ref Timer timer,
				in Tower tower,
				in Target target,
				in TowerRotation towerRotation) => {
					timer.CurrentTime += Time.DeltaTime;
					if (timer.CurrentTime < timer.TargetTime)
					{
						return;
					}

					for (int i = 0; i < (int)(timer.CurrentTime / timer.TargetTime); i++)
					{
						var id = GameController.Singleton.NewID;
						var newBullet = comandBuffer.Instantiate(tower.BulletEntity);

						comandBuffer.SetComponent(newBullet, new Translation {
							Value = towerRotation.CurrentShotPoint
						});
						comandBuffer.AddComponent(newBullet, new VelocitySpeed {
							SpeedValue = tower.Speed
						});
						comandBuffer.AddComponent(newBullet, new Bullet {
							ID = id,
							Damage = tower.Damage
						});
						comandBuffer.AddComponent(newBullet, new Target {
							TargetValue = target.TargetValue,
							CurrentTargetID = target.CurrentTargetID
						});

						GameController.Singleton.FrameManager.AddObjectHistory(new BulletHystory(id, tower.BulletType,
							tower.Speed,
							tower.Damage,
							target.CurrentTargetID
						));
					}
					timer.CurrentTime %= timer.TargetTime;
				}
			).WithName("TowerShotSystem").WithoutBurst().Run();

			simulationEntityCommandBuffer.AddJobHandleForProducer(Dependency);
		}
	}
}
