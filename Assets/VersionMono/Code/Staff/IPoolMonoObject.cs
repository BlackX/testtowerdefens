﻿using UnityEngine;

namespace StaffCommon
{
	public interface IPoolMonoObject
	{
		MonoPoolTypes ObjectType { get; }
		MonoBehaviour Mono { get; }
		void BeforePush();
		void AfterPull();
	}
}

