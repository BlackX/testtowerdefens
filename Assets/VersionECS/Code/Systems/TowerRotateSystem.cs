﻿using Unity.Transforms;
using Unity.Entities;
using UnityEngine;

namespace ECSTowerDefense.Systems
{
	[UpdateInGroup(typeof(SimulationSystemGroup)), UpdateBefore(typeof(TowerShotSystem))]
	public class TowerRotateSystem: SystemBase
	{
		protected override void OnUpdate()
		{
			var translationType = GetComponentDataFromEntity<Translation>(true);

			Entities.ForEach((
				ref Rotation rotation,
				ref TowerRotation towerRotation,
				in Translation translation,
				in Target target) => {
					if (translationType.HasComponent(target.TargetValue) == false)
					{
						Debug.LogError($"Tower target entity {target.TargetValue} hoven't Translation component");
						return;
					}
					rotation.Value = StaffFunctions.GetYRotationOnTarget(
						translation.Value - translationType[target.TargetValue].Value, towerRotation.Incline);

					towerRotation.CurrentShotPoint = StaffFunctions.RotateAroundPoint(
						translation.Value + towerRotation.ShotPointOffset, translation.Value, rotation.Value);
				}
			).WithReadOnly(translationType).WithName("TowerRotateSystem").ScheduleParallel();
		}
	}
}
