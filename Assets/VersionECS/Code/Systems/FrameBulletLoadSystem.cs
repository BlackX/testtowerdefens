﻿using System.Linq;
using Unity.Transforms;
using Unity.Entities;

namespace ECSTowerDefense.Systems
{
	[DisableAutoCreation]
	public class FrameBulletLoadSystem: SystemBase
	{
		private EntityQuery query;
		private EntityQuery enemyQuery;


		protected override void OnCreate()
		{
			enemyQuery = GetEntityQuery(
				ComponentType.ReadOnly<Enemy>(),
				ComponentType.ReadOnly<AliveTag>());
		}

		protected override void OnUpdate()
		{
			var entites = enemyQuery.ToEntityArray(Unity.Collections.Allocator.TempJob);
			var enemes = enemyQuery.ToComponentDataArray<Enemy>(Unity.Collections.Allocator.TempJob);

			var frameManager = GameController.Singleton.FrameManager;
			var frameEnemes = frameManager.GetCurrentObjectsByType(EntityObjectTypes.BulletCannonBall)
				.Select(x => (BulletHystory)x).ToList();

			Entities.WithSharedComponentFilter(new ObjectType { Type = EntityObjectTypes.BulletCannonBall })
				.WithStoreEntityQueryInField(ref query).ForEach((Entity entity, int entityInQueryIndex,
				ref Bullet bullet,
				ref VelocitySpeed speed,
				ref Translation position) => {
					if (entityInQueryIndex < frameEnemes.Count)
					{
						position.Value = frameEnemes[entityInQueryIndex].GetPositionInFrame(frameManager.FrameNumber);
						speed.SpeedValue = frameEnemes[entityInQueryIndex].Speed;
						bullet.Damage = frameEnemes[entityInQueryIndex].Damage;
						bullet.ID = frameEnemes[entityInQueryIndex].ID;

						Entity currentTarget = default;
						for (int i = 0; i < entites.Length; i++)
						{
							if (enemes[i].ID == frameEnemes[entityInQueryIndex].TargetID)
							{
								currentTarget = entites[i];
							}
						}

						EntityManager.SetComponentData(entity, new Target {
							CurrentTargetID = frameEnemes[entityInQueryIndex].TargetID,
							TargetValue = currentTarget
						});
					}
					else
					{
						EntityManager.DestroyEntity(entity);
					}
				}
			).WithName("FrameBulletLoadSystem").WithStructuralChanges().WithoutBurst().Run();

			var queryCount = query.CalculateEntityCount();
			for (int i = 0; i < frameEnemes.Count - queryCount; i++)
			{
				var newBullet = EntityManager.Instantiate(GameController.Singleton.DefaultBulletEntity);

				EntityManager.SetComponentData(newBullet, new Translation {
					Value = frameEnemes[queryCount + i].GetPositionInFrame(frameManager.FrameNumber)
				});
				EntityManager.AddComponentData(newBullet, new VelocitySpeed {
					SpeedValue = frameEnemes[queryCount + i].Speed
				});
				EntityManager.AddComponentData(newBullet, new Bullet {
					ID = frameEnemes[queryCount + i].ID,
					Damage = frameEnemes[queryCount + i].Damage
				});

				Entity currentTarget = default;
				for (int e = 0; e < entites.Length; e++)
				{
					if (enemes[e].ID == frameEnemes[queryCount + e].TargetID)
					{
						currentTarget = entites[e];
					}
				}

				EntityManager.AddComponentData(newBullet, new Target {
					CurrentTargetID = frameEnemes[queryCount + i].TargetID,
					TargetValue = currentTarget
				});
			}

			entites.Dispose();
			enemes.Dispose();
		}
	}
}
