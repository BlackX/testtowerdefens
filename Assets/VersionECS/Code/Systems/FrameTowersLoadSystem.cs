﻿using System.Linq;
using Unity.Transforms;
using Unity.Entities;

namespace ECSTowerDefense.Systems
{
	[DisableAutoCreation]
	public class FrameTowersLoadSystem: SystemBase
	{
		private EntityQuery query;


		protected override void OnCreate()
		{
			query = GetEntityQuery(
				ComponentType.ReadOnly<Enemy>(),
				ComponentType.ReadOnly<AliveTag>());
		}

		protected override void OnUpdate()
		{
			var entites = query.ToEntityArray(Unity.Collections.Allocator.TempJob);
			var enemes = query.ToComponentDataArray<Enemy>(Unity.Collections.Allocator.TempJob);

			var frameManager = GameController.Singleton.FrameManager;
			var frameEnemes = frameManager.GetCurrentObjectsByType(EntityObjectTypes.TowerCannon)
				.Select(x => (TowerHystory)x).ToList();

			Entities.WithSharedComponentFilter(new ObjectType { Type = EntityObjectTypes.TowerCannon })
				.ForEach((Entity entity, int entityInQueryIndex,
				ref Timer timer,
				ref Tower tower,
				ref Translation position,
				ref TowerRotation towerRotation) => {
					tower.ID = frameEnemes[entityInQueryIndex].ID;
					tower.Speed = frameEnemes[entityInQueryIndex].Speed;
					tower.Damage = frameEnemes[entityInQueryIndex].Damage;
					tower.AttackDistance = frameEnemes[entityInQueryIndex].Distance;
					tower.BulletEntity = frameEnemes[entityInQueryIndex].BulletEntity;
					tower.BulletType = frameEnemes[entityInQueryIndex].BulletType;

					towerRotation.Incline = frameEnemes[entityInQueryIndex].Incline;
					towerRotation.ShotPointOffset = frameEnemes[entityInQueryIndex].ShotPointOffset;

					position.Value = frameEnemes[entityInQueryIndex].Position;

					timer.TargetTime = frameEnemes[entityInQueryIndex].ShotTime;
					timer.CurrentTime = frameEnemes[entityInQueryIndex].GetTimerInFrame(frameManager.FrameNumber);

					var frameTargetID = frameEnemes[entityInQueryIndex].GetTargetIdInFrame(frameManager.FrameNumber);
					if (frameTargetID != -1)
					{
						Entity currentTarget = default;
						for (int i = 0; i < entites.Length; i++)
						{
							if (enemes[i].ID == frameTargetID)
							{
								currentTarget = entites[i];
							}
						}

						EntityManager.AddComponent(entity, typeof(Target));
						EntityManager.AddComponentData(entity, new Target {
							CurrentTargetID = frameTargetID,
							TargetValue = currentTarget
						});
					}
					else
					{
						EntityManager.RemoveComponent(entity, typeof(Target));
					}
				}
			).WithName("FrameTowersLoadSystem").WithStructuralChanges().WithoutBurst().Run();

			entites.Dispose();
			enemes.Dispose();
		}
	}
}
