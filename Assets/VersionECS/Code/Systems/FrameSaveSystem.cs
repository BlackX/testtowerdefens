﻿using Unity.Transforms;
using Unity.Entities;
using UnityEngine;

namespace ECSTowerDefense.Systems
{
	[UpdateInGroup(typeof(PresentationSystemGroup))]
	public class FrameSaveSystem: SystemBase
	{
		protected override void OnUpdate()
		{
			var frames = GameController.Singleton.FrameManager;

			Entities.ForEach((
				in Bullet bullet,
				in Translation translation) => {
					if (frames.GetObjectByID(bullet.ID) is BulletHystory bulletState)
					{
						bulletState.AddFrame(translation.Value);
						return;
					}
					Debug.LogError($"Frame history return invalid object for bullet with ID {bullet.ID}");
				}
			).WithName("BulletPassInFrameSaveSystem").WithoutBurst().Run();

			Entities.ForEach((
				in Timer timer,
				in Spawner spawner) => {
					if (frames.GetObjectByID(spawner.ID) is SpawnerHystory spawnerState)
					{
						spawnerState.AddFrame(timer.CurrentTime);
						return;
					}
					Debug.LogError($"Frame history return invalid object for spawner with ID {spawner.ID}");
				}
			).WithName("SpawnerPassInFrameSaveSystem").WithoutBurst().Run();

			Entities.ForEach((
				in Target target,
				in Timer timer,
				in Tower tower) => {
					if (frames.GetObjectByID(tower.ID) is TowerHystory towerState)
					{
						towerState.AddFrame(target.CurrentTargetID, timer.CurrentTime);
						return;
					}
					Debug.LogError($"Frame history return invalid object for tower with ID {tower.ID}");
				}
			).WithName("TowerWithTargetPassInFrameSaveSystem").WithoutBurst().Run();

			Entities.ForEach((
				in Timer timer,
				in Tower tower) => {
					if (frames.GetObjectByID(tower.ID) is TowerHystory towerState)
					{
						towerState.AddFrame(-1, timer.CurrentTime);
						return;
					}
					Debug.LogError($"Frame history return invalid object for tower with ID {tower.ID}");
				}
			).WithName("TowerWithoutTargetPassInFrameSaveSystem").WithoutBurst().Run();

			Entities.WithAll<InGameTag>().ForEach((
				in Enemy enemy,
				in Health health,
				in Translation position,
				in AnimatorData animator) => {
					if (frames.GetObjectByID(enemy.ID) is EnemyHystory enemyState)
					{
						enemyState.AddFrame(position.Value, health.CurrenttHelth, 
							animator.Animator.GetCurrentAnimatorStateInfo(0).normalizedTime);
						return;
					}
					Debug.LogError($"Frame history return invalid object for enemy with ID {enemy.ID}");
				}
			).WithName("EnemyPassInFrameSaveSystem").WithoutBurst().Run();

			frames.UpdateFrameData(Time.DeltaTime);
		}
	}
}
