﻿using UnityEngine;
using StaffCommon;

namespace MonoTowerDefense.Rewind.Objects
{
	public class Enemy: MonoBehaviour, IPoolMonoObject, IWithState
	{
		[SerializeField]
		private MonoPoolTypes poolObjectType = MonoPoolTypes.Goblin;
		[SerializeField]
		private float health = 3f;
		[SerializeField]
		private float speed = 1f;
		[SerializeField]
		private float animationSpeed = 0.75f;
		[SerializeField]
		private Transform heartPoint = default;
		[SerializeField]
		private Animator animator = default;

		private float currentHealth;
		private float targetDistanse;

		private Vector3 targetPosition;

		public Vector3 HeartPoint => heartPoint.position;
		public bool Alive => currentHealth > 0f;


		private void Awake()
		{
			targetPosition = GameController.Singleton.TargetPosition;
			animator.speed = speed * animationSpeed;
		}

		private void Update()
		{
			if (Alive == true)
			{
				if (targetDistanse < 0f)
				{
					animator.StopPlayback();
					FramesManager.Singleton.ReturnObjectToPool(this);
				}
				else
				{
					transform.Translate(new Vector3(0f, 0f, speed * Time.deltaTime));
					targetDistanse -= speed * Time.deltaTime;
				}
			}
			else if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
			{
				animator.StopPlayback();
				FramesManager.Singleton.ReturnObjectToPool(this);
			}
		}

		private void OnValidate()
		{
			animationSpeed = (animationSpeed < 0.1f) ? 0.1f : animationSpeed;
			speed = (speed < 0.1f) ? 0.1f : speed;
			health = (health < 0.1f) ? 0.1f : health;

			if (heartPoint == null)
			{
				Debug.LogError("HeartPoint in enemy cannot be empty. Click me!", this);
			}
			if (animator == null)
			{
				Debug.LogError("Animator in enemy cannot be empty. Click me!", this);
			}
		}


		public MonoPoolTypes ObjectType => poolObjectType;
		public MonoBehaviour Mono => this;

		public void BeforePush() { }

		public void AfterPull()
		{
			currentHealth = health;
			animator.Play("Goblin_run");
			transform.rotation = Quaternion.Euler(0f, -90f, 0f);
		}


		public int ID { get; set; }

		public object[] SaveState()
		{
			var _animatorState = animator.GetCurrentAnimatorStateInfo(0);
			var currentAnimationTime = _animatorState.normalizedTime % 1;

			return new object[] { ID, currentHealth, transform.position, _animatorState.shortNameHash, currentAnimationTime };
		}

		public void LoadState(object[] _data)
		{
			ID = (int)_data[0];
			currentHealth = (float)_data[1];
			transform.position = (Vector3)_data[2];
			animator.Play((int)_data[3], 0, (float)_data[4]);

			UpdateTargetDistanse();
		}


		public void UpdateTargetDistanse()
		{
			targetDistanse = (targetPosition - transform.position).magnitude;
		}

		public void Damage(float _damage)
		{
			currentHealth -= _damage;
			if (currentHealth <= 0)
			{
				animator.Play("Goblin_death");
			}
		}
	}
}
