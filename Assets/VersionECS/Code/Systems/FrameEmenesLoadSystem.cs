﻿using System.Linq;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Entities;

namespace ECSTowerDefense.Systems
{
	[DisableAutoCreation]
	public class FrameEmenesLoadSystem: SystemBase
	{
		private EntityQuery enemesInGameQuery;
		private EntityQuery enemesOutGameQuery;

		protected override void OnUpdate()
		{
			var frameManager = GameController.Singleton.FrameManager;
			var frameEnemes = frameManager.GetCurrentObjectsByType(EntityObjectTypes.GoblinEnemy)
				.Select(x => (EnemyHystory)x).ToList();

			var inGameCount = enemesInGameQuery.CalculateEntityCount();

			Entities.WithAll<InGameTag>().WithSharedComponentFilter(new ObjectType { Type = EntityObjectTypes.GoblinEnemy })
				.WithStoreEntityQueryInField(ref enemesInGameQuery).ForEach((Entity entity, int entityInQueryIndex,
				ref Enemy enemy,
				ref Health health,
				ref Translation position,
				in AnimatorData animator) => {
					if (entityInQueryIndex < frameEnemes.Count)
					{
						position.Value = frameEnemes[entityInQueryIndex].GetPositionInFrame(frameManager.FrameNumber);
						enemy.DistanceToTarget = math.distance(position.Value, GameController.Singleton.TargetPosition);
						enemy.ID = frameEnemes[entityInQueryIndex].ID;

						health.CurrenttHelth = frameEnemes[entityInQueryIndex].GetHealthInFrame(frameManager.FrameNumber);
						if (health.CurrenttHelth > 0f)
						{
							EntityManager.AddComponent(entity, typeof(AliveTag));
							animator.Animator.Play(animator.RunAnimationName, 0,
								frameEnemes[entityInQueryIndex].GetAnimationTimeInFrame(frameManager.FrameNumber));
						}
						else
						{
							EntityManager.RemoveComponent(entity, typeof(AliveTag));
							animator.Animator.Play(animator.DeathAnimationName, 0,
								frameEnemes[entityInQueryIndex].GetAnimationTimeInFrame(frameManager.FrameNumber));
						}
					}
					else
					{
						animator.Animator.gameObject.SetActive(false);
						Staff.PoolEntityObjects.Singleton.Push(entity, animator, EntityObjectTypes.GoblinEnemy);
						EntityManager.RemoveComponent(entity, typeof(AliveTag));
						EntityManager.RemoveComponent(entity, typeof(InGameTag));
					}
				}
			).WithName("InGamePassInFrameEmenesLoadSystem").WithoutBurst().WithStructuralChanges().Run();

			Entities.WithNone<InGameTag>().WithSharedComponentFilter(new ObjectType { Type = EntityObjectTypes.GoblinEnemy })
				.WithStoreEntityQueryInField(ref enemesOutGameQuery).ForEach((Entity entity, int entityInQueryIndex,
				ref Enemy enemy,
				ref Health health,
				ref Translation position) => {
					if (entityInQueryIndex + inGameCount < frameEnemes.Count)
					{
						var (enemyEntity, animatorData) = Staff.PoolEntityObjects.Singleton.Pull(EntityObjectTypes.GoblinEnemy);
						animatorData.Animator.gameObject.SetActive(true);

						position.Value = frameEnemes[entityInQueryIndex + inGameCount].GetPositionInFrame(frameManager.FrameNumber);
						enemy.DistanceToTarget = math.distance(position.Value, GameController.Singleton.TargetPosition);
						enemy.ID = frameEnemes[entityInQueryIndex].ID;

						health.CurrenttHelth = health.StartHelth;
						if (health.CurrenttHelth > 0f)
						{
							EntityManager.AddComponent(entity, typeof(AliveTag));
							animatorData.Animator.Play(animatorData.RunAnimationName, 0,
								frameEnemes[entityInQueryIndex].GetAnimationTimeInFrame(frameManager.FrameNumber));
						}
						else
						{
							animatorData.Animator.Play(animatorData.DeathAnimationName, 0,
								frameEnemes[entityInQueryIndex].GetAnimationTimeInFrame(frameManager.FrameNumber));
						}
					}
				}
			).WithName("OutGamePassInFrameEnemesLoadSystem").WithoutBurst().WithStructuralChanges().Run();
		}
	}
}
