﻿using Unity.Entities;
using UnityEngine;

namespace ECSTowerDefense
{
	[RequiresEntityConversion]
	public class SpawnerAuthoring: MonoBehaviour, IConvertGameObjectToEntity
	{
		[SerializeField]
		private float spawnTime = 1f;
		[SerializeField]
		private EntityObjectTypes objectType = EntityObjectTypes.TimerSpawner;
		[SerializeField]
		private EntityObjectTypes spawnObjectType = EntityObjectTypes.GoblinEnemy;


		private void OnValidate()
		{
			spawnTime = (spawnTime < 0.1f) ? 0.1f : spawnTime;
		}


		public void Convert(Entity _entity, EntityManager _manager, GameObjectConversionSystem _system)
		{
			_manager.AddComponentData(_entity, new Timer {
				TargetTime = spawnTime,
				CurrentTime = 0f
			});

			var id = GameController.Singleton.NewID;
			_manager.AddComponentData(_entity, new Spawner {
				ID = id,
				SpawnObjectType = spawnObjectType
			});

			_manager.AddSharedComponentData(_entity, new ObjectType {
				Type = objectType
			});

			GameController.Singleton.FrameManager.AddObjectHistory(new SpawnerHystory(id, objectType,
				transform.position,
				spawnObjectType,
				spawnTime
			));
		}
	}
}
