﻿using Unity.Mathematics;
using Unity.Transforms;
using Unity.Entities;
using Unity.Jobs;

namespace ECSTowerDefense.Systems
{
	[UpdateInGroup(typeof(TransformSystemGroup))]
	public class EnemyTranslateSystem: SystemBase
	{
		protected override void OnUpdate()
		{
			var deltaTime = Time.DeltaTime;

			Entities.WithAll<AliveTag>().ForEach((
				ref Enemy enemy,
				ref Translation translation,
				in VelocitySpeed speed) => {
					translation.Value += deltaTime * speed.SpeedValue * new float3(-1f, 0f, 0f);
					enemy.DistanceToTarget -= speed.SpeedValue * deltaTime;
				}
			).WithName("EnemyTranslateSystem").ScheduleParallel();
		}
	}
}
