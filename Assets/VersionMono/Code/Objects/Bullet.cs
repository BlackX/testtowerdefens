﻿using System.Linq;
using UnityEngine;
using StaffCommon;

namespace MonoTowerDefense.Rewind.Objects
{
	public class Bullet: MonoBehaviour, IPoolMonoObject, IWithState
	{
		[SerializeField]
		private MonoPoolTypes gameObjectType = MonoPoolTypes.CannonBall;
		[SerializeField]
		private float speed = 3f;

		private Enemy target;
		private float damage;
		private MonoPoolTypes enemyObjectType = MonoPoolTypes.Goblin;


		private void Update()
		{
			if (target == null || target.gameObject.activeSelf == false)
			{
				FramesManager.Singleton.ReturnObjectToPool(this);
				return;
			}
			if ((target.HeartPoint - transform.position).magnitude < speed * Time.smoothDeltaTime)
			{
				target.Damage(damage);
				FramesManager.Singleton.ReturnObjectToPool(this);
			}
			else
			{
				transform.LookAt(target.HeartPoint);
				transform.Translate(0f, 0f, speed * Time.deltaTime);
			}
		}

		private void OnValidate()
		{
			speed = (speed < 0.1f) ? 0.1f : speed;
		}


		public MonoPoolTypes ObjectType => gameObjectType;
		public MonoBehaviour Mono => this;

		public void BeforePush() { }

		public void AfterPull() { }


		public int ID { get; set; }

		public object[] SaveState()
		{
			return new object[] { ID, damage, transform.position, enemyObjectType, target.ID };
		}

		public void LoadState(object[] _data)
		{
			ID = (int)_data[0];
			damage = (float)_data[1];
			transform.position = (Vector3)_data[2];
			enemyObjectType = (MonoPoolTypes)_data[3];

			target = FramesManager.Singleton.GetObjectsByType(enemyObjectType).First(x => x.ID == (int)_data[4]) as Enemy;
		}


		public void SetParametrs(float _damage, Enemy _target)
		{
			damage = _damage;
			target = _target;
			enemyObjectType = target.ObjectType;
		}
	}
}
