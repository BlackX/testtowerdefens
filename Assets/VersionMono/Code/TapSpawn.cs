﻿using UnityEngine;
using StaffCommon;
using MonoTowerDefense.Rewind;
using MonoTowerDefense.Rewind.Objects;

namespace MonoTowerDefense
{
	public class TapSpawn: MonoBehaviour
	{
		[SerializeField]
		private MonoPoolTypes spawnObjectType = MonoPoolTypes.Goblin;


		private void Update()
		{
			if (GameController.Singleton.IsPause == true)
			{
				return;
			}

			if (Input.GetMouseButtonDown(0) == true)
			{
				SpawnFromScreenCoordintes(Input.mousePosition);
			}

			for (int i = 0; i < Input.touchCount; i++)
			{
				if (Input.GetTouch(i).phase == TouchPhase.Began)
				{
					SpawnFromScreenCoordintes(Input.GetTouch(i).position);
				}
			}
		}


		private void SpawnFromScreenCoordintes(Vector2 _screenCoordinates)
		{
			if (Physics.Raycast(Camera.main.ScreenPointToRay(_screenCoordinates), out RaycastHit _hit))
			{
				if (FramesManager.Singleton.CreateCurrentObjectFromPool(spawnObjectType) is Enemy _newEnemy)
				{
					_newEnemy.transform.position = _hit.point;
					_newEnemy.UpdateTargetDistanse();
				}
				else
				{
					Debug.LogError($"Object manager return invalid enemy from type: {spawnObjectType}");
				}
			}
		}
	}
}
