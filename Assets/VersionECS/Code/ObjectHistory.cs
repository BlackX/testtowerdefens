﻿using System.Collections.Generic;
using UnityEngine;

namespace ECSTowerDefense
{
	public abstract class ObjectHistory
	{
		public int ID { get; private set; }
		public EntityObjectTypes Type { get; private set; }

		public int EndFrame { get; set; }
		public int StartFrame { get; private set; }

		protected int currentFrame;


		public ObjectHistory(int _id, EntityObjectTypes _type, int _createFrame = 0)
		{
			ID = _id;
			Type = _type;
			currentFrame = 0;
			EndFrame = _createFrame;
			StartFrame = _createFrame;
		}

		public void SetStartFrame(int _frameNumber)
		{
			if (_frameNumber < 0)
			{
				Debug.LogError($"Negative number {_frameNumber} cannot be start frame");
			}
			EndFrame = _frameNumber;
			StartFrame = _frameNumber;
		}

		public void SetCurrentFrame(int _frameNumber)
		{
			if (CheckFrameNumber(_frameNumber) == false)
			{
				Debug.LogError($"Object don't exist int frame {EndFrame}");
			}
			currentFrame = _frameNumber - StartFrame;
		}

		public void UpdateFrame()
		{
			currentFrame += 1;
			EndFrame += 1;
		}

		public bool CheckFrameNumber(int _frameNumber)
		{
			return _frameNumber >= StartFrame && _frameNumber <= EndFrame;
		}


		protected void AddFrameValue<T>(List<T> _list, T _newValue)
		{
			if (currentFrame < _list.Count)
			{
				_list[currentFrame] = _newValue;
			}
			else
			{
				_list.Add(_newValue);
			}
		}
	}
}
