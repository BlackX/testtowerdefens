﻿using System;
using UnityEngine;

namespace StaffCommon
{
    public abstract class MonoSingleton<T>: MonoBehaviour where T : MonoSingleton<T>
    {
        [NonSerialized]
        private static T singleton;
        public static T Singleton
        {
            get
            {
                if (singleton == null)
                {
                    FindObjectOfType<T>()?.Awake();
                    if (singleton == null)
                    {
                        Debug.LogError($"Try get not exist single: {typeof(T)}");
                        return null;
                    }
                }
                return singleton;
            }
        }


        private void Awake()
        {
            if (singleton == null)
            {
                singleton = (T)this;
                AwakeFunction();
            }
            if (singleton != this)
            {
                Debug.LogWarning($"[Single] Try set another '{GetType()}'");
                DestroyImmediate(gameObject);
            }
        }

        private void OnDestroy()
        {
            if (singleton == (T)this)
            {
                singleton = null;
            }
            DesrtoyFunction();
        }


        protected abstract void AwakeFunction();
        protected abstract void DesrtoyFunction();
    }
}
