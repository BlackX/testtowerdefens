﻿using System.Linq;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace ECSTowerDefense.Staff
{
	public class PoolEntityObjects: MonoSingleton<PoolEntityObjects>
	{
		[System.Serializable]
		private class PoolObjectSettings
		{
			[SerializeField]
			private EntityObjectTypes objectType = default;
			public EntityObjectTypes ObjectType => objectType;
			[SerializeField]
			private GameObject objectPrefab = default;
			public GameObject ObjectPrefab => objectPrefab;
			[SerializeField]
			private int startCount = 5;
			public int StartCount => startCount;
			[SerializeField]
			private int minCount = 2;
			public int MinCount => minCount;
		}


		[SerializeField]
		private List<PoolObjectSettings> objectSettings = default;

		private Dictionary<EntityObjectTypes, PoolObjectSettings> settingsDictionary;
		private Dictionary<EntityObjectTypes, Stack<KeyValuePair<Entity, AnimatorData>>> poolDictionary;


		private void OnValidate()
		{
			if (objectSettings == null || objectSettings.Select(x => x.ObjectType).Distinct().Count() != objectSettings.Count)
			{
				Debug.LogError("ObjectSettings in Pool cannot be empty or contain non-unique types. Click me!", this);
				return;
			}

			var _setting = objectSettings.FirstOrDefault(x => x.ObjectPrefab == null);
			if (_setting != null)
			{
				Debug.LogError($"Type ({_setting.ObjectType}) in ObjectSettings in Pool have null prefab. Click me!", this);
			}
			_setting = objectSettings.FirstOrDefault(x => x.StartCount < x.MinCount);
			if (_setting != null)
			{
				Debug.LogError($"Type ({_setting.ObjectType}) in ObjectSettings in Pool count less then min count. Click me!", this);
			}
			_setting = objectSettings.FirstOrDefault(x => x.MinCount < 1);
			if (_setting != null)
			{
				Debug.LogError($"Type ({_setting.ObjectType}) in ObjectSettings in Pool have very less min count. Click me!", this);
			}
		}


		protected override void AwakeFunction()
		{
			settingsDictionary = new Dictionary<EntityObjectTypes, PoolObjectSettings>();
			poolDictionary = new Dictionary<EntityObjectTypes, Stack<KeyValuePair<Entity, AnimatorData>>>();

			foreach (var _poolObject in objectSettings)
			{
				settingsDictionary.Add(_poolObject.ObjectType, _poolObject);
				poolDictionary.Add(_poolObject.ObjectType, new Stack<KeyValuePair<Entity, AnimatorData>>());
				for (int i = 0; i < _poolObject.StartCount; i++)
				{
					CreateObject(_poolObject.ObjectType);
				}
			}
		}

		protected override void DesrtoyFunction() { }


		public (Entity, AnimatorData) Pull(EntityObjectTypes _type)
		{
			if (poolDictionary.ContainsKey(_type) == false)
			{
				Debug.LogError($"Unknown pool object type {_type} for pool settings");
				return default;
			}
			for (int i = 0; i < settingsDictionary[_type].MinCount - poolDictionary[_type].Count; i++)
			{
				CreateObject(_type);
			}
			if (poolDictionary[_type].Count == 0)
			{
				Debug.LogWarning($"Pool with type {_type} is empty :(");
				return default;
			}

			var _newPair = poolDictionary[_type].Pop();
			return (_newPair.Key, _newPair.Value);
		}

		public void Push(Entity _entity, AnimatorData _animator, EntityObjectTypes _type)
		{
			if (poolDictionary.ContainsKey(_type) == false)
			{
				Debug.LogError($"Unknown pool object type {_type} for pool settings");
				return;
			}
			poolDictionary[_type].Push(new KeyValuePair<Entity, AnimatorData>(_entity, _animator));
		}

		private void CreateObject(EntityObjectTypes _type)
		{
			Instantiate(settingsDictionary[_type].ObjectPrefab, transform);
		}
	}
}
