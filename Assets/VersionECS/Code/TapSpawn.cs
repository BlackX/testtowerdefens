﻿using Unity.Entities;
using UnityEngine;
using ECSTowerDefense;

public class TapSpawn: MonoBehaviour
{
	[SerializeField]
	private EntityObjectTypes animatedObjectTypeForSpawn = EntityObjectTypes.GoblinEnemy;

	private EntityManager manager;


	private void Awake()
	{
		manager = World.DefaultGameObjectInjectionWorld.EntityManager;
	}

	private void Update()
	{
		if (GameController.Singleton.IsPause == true)
		{
			return;
		}

		if (Input.GetMouseButtonDown(0) == true)
		{
			SpawnFromScreenCoordintes(Input.mousePosition);
		}

		for (int i = 0; i < Input.touchCount; i++)
		{
			if (Input.GetTouch(i).phase == TouchPhase.Began)
			{
				SpawnFromScreenCoordintes(Input.GetTouch(i).position);
			}
		}
	}


	private void SpawnFromScreenCoordintes(Vector2 _screenCoordinates)
	{
		if (Physics.Raycast(Camera.main.ScreenPointToRay(_screenCoordinates), out RaycastHit _hit))
		{
			manager.SetComponentData(manager.CreateEntity(typeof(NewAnimatedObject)),
				new NewAnimatedObject {
					Position = _hit.point,
					Type = animatedObjectTypeForSpawn
				}
			);
		}
	}
}
