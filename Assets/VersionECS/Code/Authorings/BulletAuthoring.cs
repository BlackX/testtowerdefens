﻿using Unity.Entities;
using UnityEngine;

namespace ECSTowerDefense
{
	[RequiresEntityConversion]
	public class BulletAuthoring: MonoBehaviour, IConvertGameObjectToEntity
	{
		[SerializeField]
		private EntityObjectTypes objectType = EntityObjectTypes.BulletCannonBall;


		public void Convert(Entity _entity, EntityManager _manager, GameObjectConversionSystem _system)
		{
			_manager.AddSharedComponentData(_entity, new ObjectType {
				Type = objectType
			});
		}
	}
}
